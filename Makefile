CC = g++
CFLAGS = -O3 -Wall
LIBS = -lrlottie
DEPS = lodepng.cpp
TARGET = lfe

ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

all: $(TARGET)

$(TARGET): $(TARGET).cpp
	$(CC) $(CFLAGS) -o $(TARGET) $(DEPS) $(TARGET).cpp $(LIBS)

install: $(TARGET)
	install -m 755 $(TARGET) $(DESTDIR)$(PREFIX)/bin/

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(TARGET)

clean:
	$(RM) $(TARGET)
