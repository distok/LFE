#include "lodepng.h"
#include <rlottie.h>

#include<iostream>
#include<string>
#include<vector>
#include<array>

void encodePNG(const char* filename, std::vector<unsigned char>& image, unsigned width, unsigned height) {
  unsigned error = lodepng::encode(filename, image, width, height);
  if(error) std::cout << "encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
}

void renderFrame(size_t i, rlottie::Surface &s)
{
    uint8_t *buffer = reinterpret_cast<uint8_t *>(s.buffer());

    char filename [50];
    sprintf (filename, "frame-%ld.png", i + 1);

    std::cout << "Working on " << filename << std::endl;

    unsigned width = s.width(), height = s.height();
    std::vector<unsigned char> image;
    image.resize(width * height * 4);

    for(unsigned y = 0; y < height; y++)
    for(unsigned x = 0; x < width; x++) {
        uint32_t i = 4 * width * y + 4 * x;
        unsigned char r = buffer[i+2];
        unsigned char g = buffer[i+1];
        unsigned char b = buffer[i];
        unsigned char a = buffer[i+3];
        image[4 * width * y + 4 * x + 0] = r;
        image[4 * width * y + 4 * x + 1] = g;
        image[4 * width * y + 4 * x + 2] = b;
        image[4 * width * y + 4 * x + 3] = a;
    }

    encodePNG(filename, image, width, height);
}

void intro() {
    std::cout<<"Welcome to LFE.\n";
    std::cout<<"https://gitdab.com/distok/lfe | Licensed MIT\n\n";
}

int help() {
    std::cout<<"Usage: \n   lfe [lottieFileName] [Resolution]\n\nExamples: \n    $ lfe input.json\n    $ lfe input.json 200x200\n";
    return 1;
}

int main(int argc, char **argv)
{
    intro();
    char *path{nullptr};

    size_t w = 500;
    size_t h = 500;

    if (argc > 1) path = argv[1];
    if (argc > 2) {
        char tmp[20];
        char *x = strstr(argv[2], "x");
        if (x) {
            snprintf(tmp, x - argv[2] + 1, "%s", argv[2]);
            w = atoi(tmp);
            snprintf(tmp, sizeof(tmp), "%s", x + 1);
            h = atoi(tmp);
        }
    }

    if (!path) return help();

    auto player = rlottie::Animation::loadFromFile(path);
    if (!player) return help();
    auto buffer = std::unique_ptr<uint32_t[]>(new uint32_t[w * h]);
    size_t frameCount = player->totalFrame();

    for (size_t i = 0; i < frameCount ; i++) {
        rlottie::Surface surface(buffer.get(), w, h, w * 4);
        player->renderSync(i, surface);
        renderFrame(i, surface);
    }

    return 0;
}